import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Header, Left, Body, Right, Button, Title } from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import CompLoader from './CompLoader';
import MoreMenu from './libs/MoreMenu';
import { Provider } from 'react-native-paper';
import AboutUs from './activity/AboutUs';
import Settings from './activity/Settings';
import Home from './activity/Home';

const styler = StyleSheet.create({
	mVCont: {
		flexDirection: 'column', flex: 1
	},
	mVBody: {
		flex: 1, padding: 8
	},
	mVHeaderWra: {
		height: 58
	},
	mVMenuIc: {
		fontSize: 28, color: "#fff"
	},
	mV3DotIc: {
		fontSize: 20, color: "#fff"
	}
});

class MainView extends Component {
	state = { activity: '' };

	onOpen = () => { this.props.onOpen(); }

	on3DotPress = () => { this.mMRef.openMenu(); }

	onMenuPress = (menu) => {
		this.setActivity(menu.replace(/_/g, ''));
		this.props.setItem({});
	}

	setActivity = (activity) => { this.setState({ activity }); }

	renderHeader = (title = 'Home') => {
		return (<Header>
			<Left>
				<Button transparent onPress={this.onOpen}><Icon name='menu' style={styler.mVMenuIc} /></Button>
			</Left>
			<Body><Title>{title}</Title></Body>
			<Right>
				<Button transparent onPress={this.on3DotPress}><Icon name='dots-three-vertical' style={styler.mV3DotIc} /></Button>
			</Right>
		</Header>);
	}

	renderBody = () => { return (<CompLoader {...this.props} />); }

	render() {
		let title = (this.state.activity !== '') ? this.state.activity : (this.props.item ? this.props.item.title : 'React Native UI');
		return (<Provider>
			<View style={styler.mVCont}>
				<View style={styler.mVHeaderWra}>{this.renderHeader(title)}</View>
				<MoreMenu ref={mr => this.mMRef = mr} onMenuPress={this.onMenuPress} items={['Home','Settings', 'About_Us']} />
				{this.state.activity === '' && (<View style={styler.mVBody}>{this.renderBody()}</View>)}
				{this.state.activity === 'AboutUs' && (<AboutUs />)}
				{this.state.activity === 'Settings' && (<Settings />)}
				{this.state.activity === 'Home' && (<Home />)}
			</View>
		</Provider>);
	}
}

export default MainView;
