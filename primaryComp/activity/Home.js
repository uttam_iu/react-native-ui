import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

const styler = StyleSheet.create({
	header: {
		textAlign: "center", fontSize: 26, color: "rgba(0,0,0,1)"
	},
	marginTop10: {
		marginTop: 10
	},
	marginTop20: {
		marginTop: 20
	},
	bHeader: {
		fontSize: 16, color: "rgba(0,0,0,1)"
	},
	bBody: {
		fontSize: 14, color: "rgba(0,0,0,0.5)", textAlign: "justify"
	}
});

class WelcomeScreen extends Component {
	renderHeader = () => {
		return <Text style={styler.header}>Welcome to React-Native</Text>
	}

	renderBody = () => {
		return (<View>
			<Text style={styler.bHeader}>Create native apps for Android and iOS using React</Text>
			<Text style={styler.bBody}>
				React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.
            </Text>
			<Text style={[styler.bBody, styler.marginTop10]}>
				Use a little—or a lot. You can use React Native today in your existing Android and iOS projects or you can create a whole new app from scratch.
            </Text>

			<Text style={[styler.bHeader, styler.marginTop20]}>Written in JavaScript—rendered with native code</Text>
			<Text style={styler.bBody}>
				React primitives render to native platform UI, meaning your app uses the same native platform APIs other apps do.
            </Text>
			<Text style={[styler.bBody, styler.marginTop10]}>
				Many platforms, one React. Create platform-specific versions of components so a single codebase can share code across platforms. With React Native, one team can maintain two platforms and share a common technology—React.
            </Text>

			<Text style={[styler.bHeader, styler.marginTop20]}>Native Development For Everyone</Text>
			<Text style={styler.bBody}>
				React Native lets you create truly native apps and doesn't compromise your users' experiences. It provides a core set of platform agnostic native components like View, Text, and Image that map directly to the platform’s native UI building blocks.
             </Text>

			<Text style={[styler.bHeader, styler.marginTop20]}>Seamless Cross-Platform</Text>
			<Text style={styler.bBody}>
				React components wrap existing native code and interact with native APIs via React’s declarative UI paradigm and JavaScript. This enables native app development for whole new teams of developers, and can let existing native teams work much faster.
             </Text>

			<Text style={[styler.bHeader, styler.marginTop20]}>Fast Refresh</Text>
			<Text style={styler.bBody}>
				See your changes as soon as you save. With the power of JavaScript, React Native lets you iterate at lightning speed. No more waiting for native builds to finish. Save, see, repeat.
             </Text>

			<Text style={[styler.bHeader, styler.marginTop20]}>Facebook Supported, Community Driven</Text>
			<Text style={styler.bBody}>
				Facebook released React Native in 2015 and has been maintaining it ever since.</Text>
			<Text style={[styler.bBody, styler.marginTop10]}>
				In 2018, React Native had the 2nd highest number of contributors for any repository in GitHub. Today, React Native is supported by contributions from individuals and companies around the world including Callstack, Expo, Infinite Red, Microsoft, and Software Mansion.
            </Text>
			<Text style={[styler.bBody, styler.marginTop10]}>
				Our community is always shipping exciting new projects and exploring platforms beyond Android and iOS with repos like React Native Windows and React Native Web.
            </Text>
		</View>);
	}

	render() {
		return (<View style={{padding: 8}}>
			{this.renderHeader()}
			{this.renderBody()}
		</View>);
	}
}

export default WelcomeScreen;