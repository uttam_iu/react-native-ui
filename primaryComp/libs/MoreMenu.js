import * as React from 'react';
import { Dimensions } from 'react-native';
import { Menu, Divider } from 'react-native-paper';

class MoreMenu extends React.Component {
	state = { isOpen: false }
	openMenu = (e) => { this.setState({ isOpen: true }); }

	closeMenu = () => { this.setState({ isOpen: false }); }

	onMenuPress = (menu) => {
		this.props.onMenuPress(menu);
		this.closeMenu()
	}

	renderItem = () => {
		let items = this.props.items || [], jsxAr = [];
		items.forEach((each, i) => {
			let jsx = <Menu.Item key={i} onPress={(e) => { this.onMenuPress(each) }} title={each.replace(/_/g,' ')} />
			if (i < items.length - 1) jsxAr.push(jsx, <Divider />);
			else jsxAr.push(jsx);
		});
		return jsxAr;
	}

	render() {
		return (<Menu
			visible={this.state.isOpen}
			onDismiss={this.closeMenu}
			anchor={{ x: Dimensions.get('window').width, y: 0 }}>
			{this.renderItem()}
		</Menu>);
	}
};

export default MoreMenu;