import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import SidebarContent from './SidebarContent';
import logo from '../resources/react-native-logo.png';

const styler = StyleSheet.create({
	sBCont: {
		backgroundColor: "#f4f4f4", height: "100%"
	},
	sBHeaderCont: {
		height: 120, width: "100%", backgroundColor: "#7d8079", justifyContent: "center", padding: 16
	},
	sBSepa: {
		height: 1, width: "100%", backgroundColor: "white"
	},
	sBHWra: {
		flexDirection: "row", justifyContent: "space-between"
	},
	img: {
		height: 80, width: 80
	},
	titleWra: {
		height: 80, justifyContent: "center"
	},
	title: {
		fontSize: 28, color: "#ffffff"
	}
});

class Sidebar extends Component {

	renderHeader = () => {
		return (<View style={styler.sBHeaderCont}>
			<View style={styler.sBHWra}>
				<Image style={styler.img} source={logo} />
				<View style={styler.titleWra}>
					<Text style={styler.title}>React Native</Text>
				</View>
			</View>
		</View>);
	}

	render() {
		return (<View style={styler.sBCont}>
			{this.renderHeader()}
			<View style={styler.sBSepa} />
			<SidebarContent ref={r => this.sContRef = r} {...this.props} />
		</View>);
	}
}

export default Sidebar;