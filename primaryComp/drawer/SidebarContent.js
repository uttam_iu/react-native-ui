import React, { Component } from 'react';
import { View, Text, FlatList, Image, StyleSheet } from 'react-native';
import { Accordion } from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import commandLogo from '../resources/command-logo.png';
import firebaseLogo from '../resources/firebase-logo.png';
import nativeBaseLogo from '../resources/native-base-logo.png';
import kittenLogo from '../resources/kitten-logo.png';
import paperLogo from '../resources/paper-logo.jpg';
import reactNativeLogo from '../resources/react-native-logo.png';
import nativeElementsLogo from '../resources/native-elements-logo.png';

const styler = StyleSheet.create({
	sbCont: {
		flex: 1, backgroundColor: "#7d8079"
	},
	accWra: {
		padding: 0
	},
	accHeaderWra: {
		flexDirection: "row", padding: 10, alignItems: "center", backgroundColor: "#7d8079"
	},
	accLogo: {
		height: 24, width: 24
	},
	accText: {
		fontWeight: "600", color: "#ffffff", paddingLeft: 8, fontSize: 16
	},
	accArrowIc: {
		fontSize: 18, color: "#ffffff", flex: 1, textAlign: "right"
	},
	accContWra: {
		backgroundColor: "#e3f1f1"
	},
	itemCont: {
		height: 40, width: "100%", flexDirection: "column"
	},
	selItemBack: {
		backgroundColor: "green"
	},
	itemWra: {
		paddingLeft: 16, width: "100%", height: 39, flexDirection: "column", justifyContent: "center"
	},
	itemSep: {
		height: 1, width: "100%", backgroundColor: "green"
	}
});

class SidebarContent extends Component {
	state = { selected: {} };

	onPress = (item) => { this.setState({ selected: item }, () => { this.props.onSelect(item); }); }

	setSelected = (item) => {
		this.setState({ selected: item });
	}

	renderItem = ({ item }) => {
		let selClass = this.state.selected === item ? styler.selItemBack : '';
		return (<View style={[styler.itemCont, selClass]}>
			<View style={styler.itemWra}>
				<Text style={{ color: (this.state.selected === item) ? "white" : "black" }} onPress={() => this.onPress(item)}>{item.title}</Text>
			</View>
			<View style={styler.itemSep} />
		</View>);
	}

	renderFlatList = (items) => { return (<FlatList data={items} renderItem={this.renderItem} keyExtractor={item => item.key} />); }

	renderAccordian = (items) => { return <Accordion style={styler.accWra} dataArray={items} renderHeader={this.renderHeader} renderContent={this.renderContent} animation={true} expanded={true} /> }

	renderHeader = (item, expanded) => {
		return (<View style={styler.accHeaderWra}>
			<Image style={styler.accLogo} source={reactNativeLogo} />
			<Text style={styler.accText}>{" "}{item.title}</Text>
			{expanded ? <Icon style={styler.accArrowIc} name="up" /> : <Icon style={styler.accArrowIc} name="right" />}
		</View>);
	}

	renderContent = (item) => {
		if (!item.hasOwnProperty("child")) return null;
		return (<View style={styler.accContWra}>{this.renderFlatList(item.subMenu)}</View>);
	}

	render() { return (<View style={styler.sbCont}>{this.renderAccordian(this.props.items)}</View>); }
}

export default SidebarContent;