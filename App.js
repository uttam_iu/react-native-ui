import React, { Component } from 'react';
import { Drawer } from 'native-base';
import MainView from './primaryComp/MainView';
import Sidebar from './primaryComp/drawer/Sidebar';
import items from './menuitem.json';

class App extends Component {
	state = { item: {} };

	onClose = () => { this.dRef._root.close(); }

	onOpen = () => { this.dRef._root.open(); }

	onSelect = (item) => {
		this.mRef.setActivity('');
		this.setState({ item }, () => { this.onClose(); });
	}

	setItem = (item = {}) => {
		if (item === {}) this.sRef.sContRef.setSelected({});
		this.setState({ item });
	}

	render() {
		return (<Drawer ref={ref => this.dRef = ref} onClose={this.onClose}
			content={<Sidebar ref={sR => this.sRef = sR} items={items} onSelect={this.onSelect} />}>
			<MainView onOpen={this.onOpen} setItem={this.setItem} item={this.state.item} ref={mr => this.mRef = mr} />
		</Drawer>);
	}
}

export default App;
