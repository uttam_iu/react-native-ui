create new app: npx react-native init AwesomeProject'
start metro bunddle: npx react-native start
run in android: npx react-native run-android
run in iOS: npx react-native run-ios

to see log:
npx react-native log-ios        # For iOS
npx react-native log-android    # For Android

to see devices:
adb devices 
adb kill-server
adb start-server